# JavaFX Introduction Demos #

This is the introductory project to JavaFX. Not every topic will be covered since the library is extremely extensive. This project includes a heavily commented section for better comprehension and easy access to Oracle's JavaFX 8 resouces.

### How do I get set up? ###

* Copy the HTTPS link above in the box to the right.
* On Eclipse, File > Import > Projects from Git > Next > Clone URI > Next
* Paste the HTTPS link in the URI field > Next
* Ensure master is checked > Next > Next
* Ensure import existing Eclipse projects is selected > Next
* Ensure the project "JavaFX Introduction" is selected > Finish

### Contact ###

* Daniel Scalzi / Chandan Sarkar
* [Westhill Computer Science](http://westhillcs.com)